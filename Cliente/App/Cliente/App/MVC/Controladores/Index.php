<?php
	
	/**
	 * Clase: Index
	 */
	class Index extends Controlador {

		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}

		/**
		 * Metodo: Index
		 */
		public function Index() {

			/**
			 * Ejecutando el motor de plantillas
			 * Se muestra la plantilla HTML
			 */
			$Plantilla = new NeuralPlantillasTwig('Cliente');
			echo $Plantilla->MostrarPlantilla('Index.html');
		}

		public function consultarCliente(){
		    if(empty($_POST['condiciones'])){
                $Condiciones = [['Columna'=>"tbl_status.Nombre", 'Dato' => $_POST['status'],'Tipo' => "IGUALDAD"],
                    ['Columna'=>"tbl_informacion_usuario.FechaRegistro", 'RangoInicial'=>$_POST['fechaInicio'], 'RangoFinal'=>$_POST['fechaFinal']
                        ,'Tipo'=> "RANGO"],
                    ['Columna'=>"tbl_informacion_usuario.Edad",'Dato'=>$_POST['edad'],'Tipo'=>"COMPARACION"]];
            }else{
		        $Condiciones = json_decode($_POST['condiciones'],true);
		        $Condiciones []= ['Columna' => "tbl_informacion_usuario.Nombre", 'Dato' => $_POST['nombre'],'Tipo' => 'LIKE'];
            }
            $numRegistros = (isset($_POST['numRegistros'])) ? $_POST['numRegistros'] : 5;
            $Consulta = $this->Modelo->consultarCliente($Condiciones, $numRegistros);
		    array_pop($Condiciones);
            if(count($Consulta) > 0){
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Clientes',$Consulta['Registros']);
                $Plantilla->Parametro('Condiciones',json_encode($Condiciones));
                $Plantilla->Parametro('RegistrosTotales', $Consulta['Total']);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Cliente','FiltradoCliente.html']));
            }else if(!empty($_POST['nombre'])){
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Clientes',$Consulta['Registros']);
                $Plantilla->Parametro('Condiciones',json_encode($Condiciones));
                $Plantilla->Parametro('RegistrosTotales', $Consulta['Total']);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Cliente','FiltradoCliente.html']));
            }else{
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(['Cliente','SinRegistros.html']));
            }

        }

	}