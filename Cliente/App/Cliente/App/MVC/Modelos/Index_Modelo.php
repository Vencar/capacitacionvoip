<?php

/**
 * Clase: Index_Modelo
 */
class Index_Modelo extends Modelo {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo: Ejemplo
     */
    public function consultarCliente($Campos,$Limite) {
        $Columnas = "tbl_informacion_usuario.Nombre,Edad,FechaRegistro,tbl_status.Nombre as Status,tbl_ciudad.Nombre as Ciudad";
        $Sql = "SELECT $Columnas FROM tbl_informacion_usuario"
            ." INNER JOIN tbl_status ON tbl_informacion_usuario.idStatus = tbl_status.idStatus"
            ." INNER JOIN tbl_ciudad ON tbl_informacion_usuario.idCiudad = tbl_ciudad.idCiudad";
        $Sql.=" WHERE ";
        $Sql .= AppConsultas::crearCondiciones($Campos);
        $Consulta = $this->Conexion->prepare($Sql);
        $Consulta->execute();
        $Resultados ['Total']= $Consulta->rowCount();
        if($Limite){
            $Sql.= " LIMIT " . $Limite;
        }
        $Consulta = $this->Conexion->prepare($Sql);
        $Consulta->execute();
        $Resultados ['Registros'] = $Consulta->fetchAll(PDO::FETCH_ASSOC);
        return $Resultados;
    }

}