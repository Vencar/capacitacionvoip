<?php
class AppConsultas{
    public static function crearCondiciones($Campos){
        if($Campos == true AND is_array($Campos)){
            $Condiciones=[];
            foreach ($Campos as $Llave => $Valor){
                if($Valor['Tipo'] == 'IGUALDAD'){
                    $Condiciones[] = $Valor['Columna'] ." = '". $Valor['Dato']."'";
                }
                if($Valor['Tipo'] == 'RANGO'){
                    $Condiciones[] = $Valor['Columna'] . " BETWEEN '". $Valor['RangoInicial']. "' AND '" . $Valor['RangoFinal']."'";
                }
                if($Valor['Tipo'] == 'COMPARACION'){
                    if($Valor['Dato'] == "MAYOR"){
                        $Condiciones[] = $Valor['Columna'] . " >= " . 18;
                    }else{
                        $Condiciones[] = $Valor['Columna'] . " < " . 18;
                    }
                }
                if($Valor['Tipo'] == 'LIKE'){
                    $Condiciones[] = $Valor['Columna'] . " LIKE '%" . $Valor['Dato'] . "%'";
                }
            }
            return implode(' AND ',$Condiciones);
        }
    }
}