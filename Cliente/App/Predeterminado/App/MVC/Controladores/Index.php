<?php
	
	/**
	 * Clase: Index
	 */
	class Index extends Controlador {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo: Index
		 */
		public function Index() {
			
			/**
			 * Ejecutando el motor de plantillas
			 * Se muestra la plantilla HTML
			 */
			$Plantilla = new NeuralPlantillasTwig('Cliente');
			echo $Plantilla->MostrarPlantilla('Index.html');
		}

		public function consultarCliente(){
		    $Condiciones = [['Columna'=>"tbl_status.Nombre", '$Dato' => $_POST['status'],'Tipo' => "IGUALDAD"]];
            $Consulta = $this->Modelo->consultarCliente($Condiciones);
            Ayudas::print_r($Consulta);
            exit(0);
        }

	}