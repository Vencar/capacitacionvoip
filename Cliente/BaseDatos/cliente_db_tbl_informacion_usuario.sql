-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cliente_db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_informacion_usuario`
--

DROP TABLE IF EXISTS `tbl_informacion_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_informacion_usuario` (
  `idInformacionUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Edad` int(11) NOT NULL,
  `FechaRegistro` date NOT NULL,
  `idCiudad` int(11) NOT NULL,
  `idStatus` int(11) NOT NULL,
  PRIMARY KEY (`idInformacionUsuario`,`idCiudad`,`idStatus`),
  KEY `fk_tbl_informacion_usuario_tbl_ciudad_idx` (`idCiudad`),
  KEY `fk_tbl_informacion_usuario_tbl_status1_idx` (`idStatus`),
  CONSTRAINT `fk_tbl_informacion_usuario_tbl_ciudad` FOREIGN KEY (`idCiudad`) REFERENCES `tbl_ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_informacion_usuario_tbl_status1` FOREIGN KEY (`idStatus`) REFERENCES `tbl_status` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_informacion_usuario`
--

LOCK TABLES `tbl_informacion_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_informacion_usuario` DISABLE KEYS */;
INSERT INTO `tbl_informacion_usuario` VALUES (1,'JOSE MARIA VENCES CARBAJAL',23,'2018-03-22',1,1),(2,'JESUS TEPEC',26,'2018-03-23',1,1),(3,'MARGARITO',17,'2018-02-23',1,1),(4,'BENJAMIN',23,'2018-03-26',1,1),(5,'CARLOS LAUREANO',26,'2018-03-26',1,1),(6,'FRANCISCO ABAD',30,'2018-03-26',1,1),(7,'ERNESTO',27,'2018-03-26',1,1),(8,'GERARDO EPITACIO',25,'2018-03-26',1,1),(9,'RODOLFO SALGADO',29,'2018-03-26',1,1),(10,'JESUS ALBERTO',25,'2018-03-26',1,1),(11,'CARLOS ALBERTO',40,'2018-03-26',1,1),(12,'ANTONIO ALARCON',31,'2018-03-26',1,1),(13,'GUSTAVO ADOLFO',32,'2018-03-26',1,1),(14,'JORGE ORTEGA',43,'2018-03-26',1,1),(15,'JORGE VAZQUEZ',52,'2018-03-26',1,1),(16,'MARCOS ALEJANDRO',22,'2018-03-26',1,1),(17,'DANIEL HERNANDEZ',21,'2018-03-26',1,1),(18,'VICTOR OZUNA',21,'2018-03-26',1,1),(19,'FELIX',23,'2018-03-26',1,1),(20,'MARIA GUADALUPE',22,'2018-03-26',1,1);
/*!40000 ALTER TABLE `tbl_informacion_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-27 20:22:27
